# Tennis

## About this Kata

Your task is to write a "tennis game" containing the logic which oputputs the correct score as a string for display on the scoreboard. You can assume that the umpire pressing the button "player 1 scores" will result in a method *"wonPoint("player1")"* being called, and similarly *"wonPoint("player2")"* for the other button. Afterwards, you will get a call *"getScore()"* from the scoreboard asking what it should display. This method should return a string with the score.

### The score system with its output summarized

1. A game is won by the first player to have won at least four points in total and at least two points more than the opponent. The score is then *"Win for player1"* or *"Win for player2"*

2. The running score of each game is described in a manner peculiar to tennis: scores from zero to three ponts are described as *"Love", "Fifteen", "Thirty", and "Forty"* respectiveley. The score is then *"player1: Love, player2: Thirty"*

3. If at least three points have been scored by each player, and the scores are equal, the escore is *"Deuce"*

4. If at least three points have been scoreed by each side and a player has one more point than his opponent, the escore of the game is *"Advantage player1"* or *"Advantage player2"*

Sets and matches are out of scope, so you only need to report the score for the current game.

## Round 2

Be able to change the name of the players from "player1" to "The name in question" and "player2" to "The name of the other player".
